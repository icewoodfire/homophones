/**********************************************************************/
/*               L E S        H O M O P H O N E S                     */
/**------------------------------------------------------------------**/
/*    Petit programme pour aprendre les homophones aux éleves         */
/*    de secondaire 2                                                 */
/**------------------------------------------------------------------**/
/*    Programation    : Stéphane Lorrain                              */
/*    Idées           : Diane Roberge                                 */
/*    Dernière MAJ    : Novembre 1995                                 */
/**********************************************************************/

/*== Fichiers d'inclusion      =======================================*/

#include <dos.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>

/*== Defines =========================================================*/

#define SP 0x20
#define ON + 16 *
#define FCT 0
#define BS 8
#define ENTER 13
#define ESC 27
#define TRUE 1
#define NUM 5
#define U_ARRO 72
#define D_ARRO 80
#define FCT_2 60
#define FCT_1 59

char tag_arr[NUM]={NULL};
int  scr_arr[NUM]={200,200,200,200,200};
int Score, Total;
int Option, LastBlank=0, Modify=0;
int Music=0;

/*============== Dactylo ===========================================*/
/* affiche une string au ralenti */

void Dactylo (char string[80], int temps)
{
   char x=0,y=0;
   while (string[x] != 0)
      {
      cprintf("%c",string[x++]);
      if(Music==1) 
	{
	if(y++==2)
		{
		y=0;
		if (!kbhit()) sound(50);
		if (!kbhit()) delay(1);
		nosound();
		}
	if (!kbhit()) delay(temps);
	}
      else
	if (!kbhit()) delay(temps);
      }
}

/*============ toggle ===========================================*/

void ToggleSound(void)
{
  window(1,1,80,25);
  textattr(BLUE ON LIGHTGRAY);
  gotoxy(70,25);
  if (Music==0) {Music=1; cprintf("F2=Sons \x0E");}
	   else {Music=0; cprintf("F2=Sons  ");}
}

void ToggleIn(int x,int y)
{
ToggleSound();
window(10,9,70,18);
gotoxy(x,y);
}

/*============= AffMusic ========================================*/  

void OnOff(void)
{
  window(1,1,80,25);
  textcolor(BLUE);
  gotoxy(70,25);
  if (Music==0) cprintf("F2=Sons   ");
	   else {cprintf("F2=Sons \x0E");}
}

/*============= Desktop ===========================================*/

void desktop (void)
{
   int x,y;
   textcolor(BLUE);
   textbackground(LIGHTGRAY);
   window(1,1,80,25);
   clrscr();
   gotoxy(33,1);
   cprintf("Les homophones\r\n");
   for (y=2 ; y<25 ; y++)
      cprintf("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");
   gotoxy(1,25);
   cprintf("  \x18=Haut  \x19=Bas  ENTER=Sélectionner  ESC=Quitter            F1=Aide");
   OnOff();
}

/*============== Vide le buffer de clavier ========================*/

void VideBuffer (void)
    
    {
    unsigned short far *mot ;        // Variable mot pour accès mémoire
    mot = (unsigned short far *)MK_FP(0x0040,0x001A);
    *mot = 0x001E;
    mot = (unsigned short far *)MK_FP(0x0040,0x001C);
    *mot = 0x001E;
    }

//============== Fait Aparaître le curseur ===============

void SetCur(void)
{
union REGS regs;
regs.h.ch = 0x0B;
regs.h.cl = 0x0D;
regs.h.ah = 1;
int86(0x10, &regs, &regs);
}


//============== Fait disparaître le curseur ===============

void PasCur(void)
{
union REGS regs;
regs.h.ch = 0x20;
regs.h.ah = 1;
int86(0x10, &regs, &regs);
}


//========== GetHit =============================================

void GetHit (void)
{
  unsigned short far *mot ;
  mot = (unsigned short far *)MK_FP(0x0040,0x001A);
  *mot = 0x001E;
  mot = (unsigned short far *)MK_FP(0x0040,0x001C);
  *mot = 0x001E;

  if (getch()==FCT) 
      getch();
}

//=============== Sons pour erreur =====================

void Bip (void)
{
	sound(200);
	delay(100);
	nosound();
}
	
void BipPop(void)        
{
	int i;
	for (i=1; i<8; i++)
		{ 
		sound(i*80);
		delay(8);
		}
	nosound();
}

void BipYeah (void)
	{
	int i;
	for (i=1; i<10; i++)
		{ 
		sound(i*80);
		delay(10);
		}
	nosound();
	delay(50);
	sound(950);
	delay(50);
	nosound();
	delay(50);
	sound(720);
	delay(50);
	nosound();
	}

void BipChoo (void)
	{
	int i;
	for (i=10; i>0; i--)
		{ 
		sound(i*40);
		delay(10);
		}
	sound(40);
	delay(50);
	nosound();
	}

void BadLuck (void)
	{
	int x;
	for (x=0;x<2;x++)
		{
		delay(50);
		sound(50);
		delay(50);
		nosound();
		}
	}

//=============Accès aux fenêtres==========================

void Message (char string[50])
{
   window(10,21,57,23);
   textattr(WHITE ON BLUE);
   clrscr();
   cprintf("\r\n  ");
   Dactylo(string,25);
}

/*=================================================================*/

void PopBravo (int i)
{
   int note, y, code;
   window(15,10,65,15-i);
   textbackground(RED);
   textcolor(WHITE);
   clrscr();
   note = ( (Score * 100) / Total );
   scr_arr[Option]=note;
   
   cprintf("\r\n  Vous avez atteint un pointage de %d/%d.", Score,Total);
   cprintf("\r\n  Cela équivaut à une note de %d/100.", note);
   if (note == 100)     {
			Dactylo("\r\n  C'est une note parfaite! Félicitations!",15);
			}
   else if (note >= 90) {
			Dactylo("\r\n  C'est excellent! Félicitations!",15);
			}
   else if (note >= 80) {
			Dactylo("\r\n  C'est très bien, continuez ainsi!",15);
			}
   else if (note >= 70) {
			Dactylo("\r\n  C'est bien, mais il vous est sûrement possible",15);
			Dactylo("\r\n  de faire encore mieux.",15);
			}
   else if (note >= 60) {
			Dactylo("\r\n  Ouf! vous passez de justesse!",15);
			Dactylo("\r\n  Vous pourriez réessayer pour faire mieux!",15);
			}
   else                 {
			Dactylo("\r\n  Désolé, mais vous ne passez pas...",15);
			Dactylo("\r\n  Vous pouvez sûrement faire mieux, réessayez!",15);
			}
   delay(1000);
   VideBuffer();
   Message("Appuyez sur une touche pour continuer...");
     do
	{
	code=0;
	if (getch() == 0)
	   if (getch()==FCT_2) {code=1; ToggleSound();}
	}
     while (code==1);
}       

/*============== Bravo! félicitation! ...  ========================*/

void Bravo (void)
{
	delay(500);
	int buff[80][25];
	int factor, i;

	if ( (Score * 100) / Total >=80 ) i=1;
	else i=0;
	
	gettext(15,9,66,16-i,buff);
	textbackground(RED);
	for (factor=7; factor>=0; factor--)
	    {
	    window(15+(factor*2),10+(factor/3),65-(factor*2),15-(factor/3)-i);
	    clrscr();
	    if (Music==1) {
		sound((8-factor)*100);
		delay(10);
		sound((8-factor)*100+50);
		delay(10); }
	    else delay(20);
	    }
	
	nosound();
	textbackground(BLACK);
	window(16,16-i,66,16-i);
	clrscr();
	window(66,11,66,15-i);
	clrscr();
	PopBravo(i);
	
	window(15,9,66,16-i);     // remet le texte qui était caché
	puttext(15,9,66,16-i,buff);
	
	if (Music==1)
	   { for (factor=15; factor > 0; factor--)
	     { sound(factor*50); delay(7);} 
	   nosound();
	   }
	delay(500);
}

//============== Message pour quitter===================

char PopPop(void)
   {
	char choix=1;
	int level;
	int buff[80][25];
	
	window(20,10,60,13);
	gettext(20,10,60,13,buff);

	PasCur();
	for (level=8; level>=0; level--)
	{
	if (Music==1) sound((9-level)*80);
	delay(10);
	window(20+(2*level),10,59-(2*level),12);
	textcolor(WHITE);
	textbackground(RED);
	clrscr();
	}
	nosound();
	
	window(21,13,60,13);
	textbackground(BLACK);
	clrscr();
	window(60,11,60,13);
	textbackground(BLACK);
	clrscr();

	window(20,10,59,12);
	textcolor(WHITE);
	textbackground(RED);
	cprintf("\r\n  Voulez-vous revenir au menu (O/N) ? ");
	do
	{
		 do
		   choix = getch();
		 while (choix==0);
		if ((choix=='o')||(choix=='O')) 
			{
			if (Music==1) { sound(50); delay(2); nosound(); }; 
			return 1; 
			}
		else if ((choix=='n')||(choix=='N')||(choix==ESC)) 
			{
			if (Music==1) { sound(50); delay(2); nosound(); };
			window(20,10,60,13);
			puttext(20,10,60,13,buff);
			textattr(BLACK ON LIGHTGRAY);
			SetCur();
			return 2;
			}
		else 
		{
		Bip();
		}
	}
	while (1);
   }    

/*===================== TEXTE DE L'AIDE EN LIGNE (exercices)==============*/

void PopAideExer (void)
{
   window(15,7,65,7);
   textcolor(WHITE);
   textbackground(YELLOW);
   clrscr();
   cprintf("          Aide à propos des exercices");
   window(15,8,65,17);
   textbackground(RED);
   textcolor(WHITE);
   cprintf("\r\n  Si vous avez de la difficulté à taper certains");
   cprintf("\r\n  caractères (par exemple les accents ou la");
   cprintf("\r\n  cédille), assurez-vous que votre clavier est");
   cprintf("\r\n  configuré en français.\r\n");
   cprintf("\r\n  Ensuite, pour savoir comment obtenir ces");
   cprintf("\r\n  symboles, consultez un professeur ou un ami");
   cprintf("\r\n  qui le sait!");
}        

/*======================= AIDE EN LIGNE (exercices)======================*/

void AideExer (int x, int y)
{
	int buff[80][25];
	int factor, code;

	PasCur();
	gettext(15,7,66,18,buff);
	textbackground(RED);
	for (factor=4; factor>=0; factor--)
	    {
	    window(15+(3*factor),8+factor,65-(3*factor),17-factor);
	    clrscr();
	    if (Music==1) {
		sound((8-factor)*100);
		delay(8);
		sound((8-factor)*100+50);
		delay(8); }
	    else delay(18);
	    }
	nosound();
	textbackground(BLACK);
	window(16,18,66,18);
	clrscr();
	window(66,8,66,17);
	clrscr();
	PopAideExer();

     VideBuffer();
     do
	{
	code=0;
	if (getch() == 0)
	   if (getch()==FCT_2) {code=1; ToggleSound();}
	}
     while (code==1);
     
	window(15,7,66,18);     // remet le texte qui était caché
	puttext(15,7,66,18,buff);
	
	if (Music==1)
	   { for (factor=15; factor > 0; factor--)
	     { sound(factor*50); delay(7);} 
	   nosound();
	   }

window(10,9,70,18);
textattr(BLACK ON LIGHTGRAY);
gotoxy(x,y);
SetCur();

}


/*===================== TEXTE DE L'AIDE EN LIGNE (MENU) ==================*/

void PopAideMenu (void)
{
   window(15,7,65,7);
   textcolor(WHITE);
   textbackground(YELLOW);
   clrscr();
   cprintf("          Aide à propos du menu principal");
   window(15,8,65,17);
   textbackground(RED);
   textcolor(WHITE);
   cprintf("\r\n  Choisissez un des exercices proposés.");
   cprintf("\r\n  Utilisez les flèches ");
   textcolor(YELLOW);
   cprintf("\"HAUT\"");
   textcolor(WHITE);
   cprintf(" et ");
   textcolor(YELLOW);
   cprintf("\"BAS\"");
   textcolor(WHITE);
   cprintf(" pour\r\n  changer l'option en surbrillance.");
   cprintf("\r\n  Ensuite, appuyez sur ");
   textcolor(YELLOW);
   cprintf("\"ENTER\"");
   textcolor(WHITE);
   cprintf(" pour la choisir.\r\n  ");
   cprintf("\r\n  À mesure que vous complétez les exercices,");
   cprintf("\r\n  vos points s'accumulent dans une petite liste");
   cprintf("\r\n  au bas de l'écran.");
}        

/*======================= AIDE EN LIGNE (menu) ==========================*/

void AideMenu (void)
{
	int buff[80][25];
	int factor, code;

	gettext(15,7,66,18,buff);
	textbackground(RED);
	for (factor=4; factor>=0; factor--)
	    {
	    window(15+(3*factor),8+factor,65-(3*factor),17-factor);
	    clrscr();
	    if (Music==1) {
		sound((8-factor)*100);
		delay(8);
		sound((8-factor)*100+50);
		delay(8); }
	    else delay(18);
	    }
	nosound();
	textbackground(BLACK);
	window(16,18,66,18);
	clrscr();
	window(66,8,66,17);
	clrscr();
	PopAideMenu();

     VideBuffer();
     do
	{
	code=0;
	if (getch() == 0)
	   if (getch()==FCT_2) {code=1; ToggleSound();}
	}
     while (code==1);
     
	window(15,7,66,18);     // remet le texte qui était caché
	puttext(15,7,66,18,buff);
	
	if (Music==1)
	   { for (factor=15; factor > 0; factor--)
	     { sound(factor*50); delay(7);} 
	   nosound();
	   }
}

/*========== POP1 (sa-ça) ==================================================*/
	
void Pop1 (void)
{
   window(15,5,65,5);
   textcolor(WHITE);
   textbackground(YELLOW);
   clrscr();
   cprintf("        Règles sur les homophones «sa, ça»");
   window(15,6,65,21);
   textbackground(GREEN);
   textcolor(YELLOW);
   cprintf("\r\n  sa: ");
   textcolor(WHITE);
   cprintf("adjectif possessif,");
   cprintf("\r\n  peut être remplacé par «ma»");
   cprintf("\r\n  Exemple: Sa joie est grande."); 
   cprintf("\r\n  (Ma joie...)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  ça: ");
   textcolor(WHITE);
   cprintf("pronom démonstratif,");
   cprintf("\r\n  peut être remplacé par «cela»");
   cprintf("\r\n  Exemple: Je crois que ça fonctionne.");
   cprintf("\r\n  (...cela fonctionne.)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  ça: ");
   textcolor(WHITE);
   cprintf("adverbe de lieu,");
   cprintf("\r\n  peut être remplacé par «ici»");
   cprintf("\r\n  Exemple: Des fleurs poussaient ça et là.");
   cprintf("\r\n  (...poussaient ici et là.)");
}        

/*========== POP2 (la-là-l'a) ==============================================*/
	
void Pop2 (void)
{
   window(15,5,65,5);
   textcolor(WHITE);
   textbackground(YELLOW);
   clrscr();
   cprintf("     Règles sur les homophones «la, là, l'a»");
   window(15,6,65,21);
   textbackground(GREEN);
   textcolor(YELLOW);
   cprintf("\r\n  la: ");
   textcolor(WHITE);
   cprintf("article,");
   cprintf("\r\n  peut être remplacé par «une»");
   cprintf("\r\n  Exemple: La punaise..."); 
   cprintf("\r\n  (Une punaise...)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  là: ");
   textcolor(WHITE);
   cprintf("adverbe de lieu,");
   cprintf("\r\n  peut être remplacé par «ici»");
   cprintf("\r\n  Exemple: La rivière commence là.");
   cprintf("\r\n  (La rivière commence ici.)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  l'a: ");
   textcolor(WHITE);
   cprintf("pronom personnel et verbe avoir,");
   cprintf("\r\n  peut être remplacé par «l'avait»");
   cprintf("\r\n  Exemple: Son chapeau, Jean l'a acheté ici.");
   cprintf("\r\n  (...Jean l'avait acheté...)");
}        

/*========== POP3 (peut-peux-peu) ==============================================*/
	
void Pop3 (void)
{
   window(15,5,65,5);
   textcolor(WHITE);
   textbackground(YELLOW);
   clrscr();
   cprintf("    Règles sur les homophones «peut, peux, peu»");
   window(15,6,65,21);
   textbackground(GREEN);
   textcolor(YELLOW);
   cprintf("\r\n  peut, peux: ");
   textcolor(WHITE);
   cprintf("verbe pouvoir,");
   cprintf("\r\n  «peut» peut être remplacé par «pouvait» (il)");
   cprintf("\r\n  «peux» peut être remplacé par «pouvais» (je, tu)");
   cprintf("\r\n  Exemple: Il peut rire avec moi. Je peux sourire.");
   cprintf("\r\n  (Il pouvait rire... Je pouvais sourire.)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  peu: ");
   textcolor(WHITE);
   cprintf("adverbe ayant le sens de \"pas beaucoup\",");
   cprintf("\r\n  Exemple: Il faut peu de poivre.");
   cprintf("\r\n  (Il n'en faut pas beaucoup.)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  peu à peu: ");
   textcolor(WHITE);
   cprintf("locution adverbiale,"); 
   cprintf("\r\n  a le sens de \"petit à petit\"");
   cprintf("\r\n  Exemple: Peu à peu, la ville s'agrandit.");
   cprintf("\r\n  (Petit à petit, la ville...)");
}        

/*=============== POP4 (ou-où) ==============================================*/
	
void Pop4 (void)
{
   window(15,5,65,5);
   textcolor(WHITE);
   textbackground(YELLOW);
   clrscr();
   cprintf("          Règles sur les homophones «ou, où»");
   window(15,6,65,21);
   textbackground(GREEN);
   textcolor(YELLOW);
   cprintf("\r\n  ou: ");
   textcolor(WHITE);
   cprintf("conjonction de coordination ");
   cprintf("\r\n  peut être remplacé par «ou bien»");
   cprintf("\r\n  Exemple: Il faut choisir entre l'un ou l'autre.");
   cprintf("\r\n  (...l'un ou bien l'autre.)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  où: ");
   textcolor(WHITE);
   cprintf("adverbe de lieu");
   cprintf("\r\n  peut être remplacé par «à quel endroit»");
   cprintf("\r\n  Exemple: Où peut-on aller manger ?");
   cprintf("\r\n  (À quel endroit peut-on...)\r\n");
   textcolor(YELLOW);
   cprintf("\r\n  où: ");
   textcolor(WHITE);
   cprintf("pronom relatif"); 
   cprintf("\r\n  Exemple: C'était l'été où il s'est marié.");
   cprintf("\r\n  (...l'été pendant lequel il...)");
}


/*========== Affiche les règles ============================================*/

void ReglePop (int x, int y)
{
	int buff[80][25];
	int factor, code;

	gettext(15,5,66,22,buff);
	PasCur();
	
	textbackground(GREEN);
	for (factor=7; factor>=0; factor--)
	    {
	    window(15+(2*factor),6+factor,65-(2*factor),21-factor);
	    clrscr();
	    if (Music==1) {
		sound((8-factor)*100);
		delay(8);
		sound((8-factor)*100+50);
		delay(8); }
	    else delay(18);
	    }
	nosound();
	textbackground(BLACK);
	window(16,22,66,22);
	clrscr();
	window(66,6,66,21);
	clrscr();
	
	switch(Option)
	   {
	   case(1): Pop1(); break;
	   case(2): Pop2(); break;
	   case(3): Pop3(); break;
	   case(4): Pop4(); break;
	   }
     
     VideBuffer();
     do
	{
	code=0;
	if (getch() == 0)
	   if (getch()==FCT_2) {code=1; ToggleSound();}
	}
     while (code==1);
     
	window(15,5,66,22);     // remet le texte qui était caché
	puttext(15,5,66,22,buff);
	
	if (Music==1)
	   { for (factor=15; factor > 0; factor--)
	     { sound(factor*50); delay(7);} 
	   nosound();
	   }

	window(10,9,70,18);     // retourne dans la fenêtre centrale
	textcolor(BLACK);
	textbackground(LIGHTGRAY);
	gotoxy(x,y);
	SetCur();
}

//========== GET STRING ==========================================

char *getstring (int x, int y, int lenght)
{
   int position;
   char key;
   static char buffer[80] = {NULL};
   for (position=0; position < 80; position++)    //rempli le tableau de 'NULL'
	buffer[position]=0;
   position=0;
   VideBuffer();
   _setcursortype(_NORMALCURSOR);
   do
   {
      gotoxy(x,y);
      key = getch();
      if (key == FCT)
	 {
	 key = getch();
	 if((key==75) && (position>0))
	     {
	     gotoxy(--x,y);  cprintf("%c",176);
	     buffer[--position] = 0;
	     }
	 else if (key==FCT_2) ToggleIn(x,y);
	 else if (key==FCT_1) AideExer(x,y);
	 }
      else
      {
	 if((key==BS) && (position>0))
	 {
	 gotoxy(--x,y);  cprintf("%c",176);
	 buffer[--position] = 0;
	 }
	 else
	 {
	 if((key!=BS) && (key!=ENTER) && (position<lenght) && (key!=ESC) && (key!=SP))
	    {
	    gotoxy(x++,y);   cprintf("%c", key);
	    if (Music==1) { sound(50); delay(2); nosound(); };
	    buffer[position++] = key;
	    }
	 else if (key==SP) ReglePop(x,y);
	 else if (key==ESC) 
	    {
	    if (PopPop() == 1)
		{ 
		buffer[0]=1; 
		key=ENTER;      // pour pouvoir sortir
		for (position=1 ; position<80 ; buffer[position++]=0);
		}
	    else {
		 window(10,9,70,18);
		 textcolor(BLACK);
		 textbackground(LIGHTGRAY);
		 gotoxy(x,y);
		 }

	    }
	 }
      }
   }
   while (key!=ENTER);
   if (position==0)
      for (position=0 ; position<80 ; buffer[position++]=0);
   
   _setcursortype(_NOCURSOR);
   return(buffer);
}

//============= Affiche le texte à remplir ================

void AfficheTexte (char (*list)[80])

{
   int x;
   
   for (x=7 ; x>=0 ; x--)
      {
      gotoxy(3,x+2);
      cprintf("%s", *(list+x) );
      }
}

/*============= Desktop pour les exercices ========================*/

void ExerDesktop (char (*list)[80], int largeur)
{
   int y;
   
   window(1,1,80,25);
   textattr(BLUE ON LIGHTGRAY);
   switch(Option)
      {
      case(1): cprintf("                  Exercices sur les homophones «sa» et «ça»\r\n");    break;
      case(2): cprintf("              Exercices sur les homophones «la», «là» et «l'a»\r\n"); break;
      case(3): cprintf("           Exercices sur les homophones «peut», «peux» et «peu»\r\n"); break;
      case(4): cprintf("                  Exercices sur les homophones «ou» et «où»\r\n");    break;
      }
   textattr(BLUE ON LIGHTGRAY);
   for (y=2 ; y<25 ; y++)
      cprintf("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");
   gotoxy(1,25);
   cprintf("  ENTER=Valider la réponse  ESPACE=Règles  ESC=Quitter      F1=Aide");
   
   window(10,9,70,18);
   textcolor(BLACK);
   textbackground(LIGHTGRAY);
   clrscr();
   AfficheTexte(list);
   
   window(10,20,57,20);
   textattr(BLACK ON CYAN);
   clrscr();
   cprintf("                    Message");
   
   window(10,21,57,23);
   textattr(WHITE ON BLUE);
   clrscr();
   
   window(60,20,70,20);
   textattr(BLACK ON CYAN);
   clrscr();
   cprintf("  Points");
   
   window(60,21,70,23);
   textattr(WHITE ON BLUE);
   clrscr();
   
   window(10-largeur,3,70+largeur,7);
   textattr(WHITE ON BLUE);
   clrscr();
   switch(Option)
      {
      case(1): Dactylo("\r\n  - Complétez les phrases suivantes avec «sa» ou «ça».",15); break;
      case(2): Dactylo("\r\n  - Complétez les phrases suivantes avec «la», «là» ou «l'a».",15); break;
      case(3): Dactylo("\r\n  - Complétez les phrases suivantes avec «peut», «peux» ou «peu».",15); break;
      case(4): Dactylo("\r\n  - Complétez les phrases suivantes avec «ou» ou «où».",15); break;
      }
   Dactylo("\r\n  - Pressez la barre d'espacement pour voir les règles.",15);
   Dactylo("\r\n  - Pressez le bouton ECHAP (ESC) pour revenir au menu.",15);
   
   window(10,9,70,18);
   textcolor(BLACK);
   textbackground(LIGHTGRAY);
   VideBuffer();
   }

//------------------------------------------------------------

void Points (void)
{
   window(60,21,70,23);
   textattr(WHITE ON BLUE);
   clrscr();
   cprintf("\r\n   %d/%d", Score,Total);
}
//------------------------------------------------------------

void MauvRep (char *String, char *Text, int x, int y, int limit)
{
   int i;
   LastBlank=0;
   strcpy(Text,"Mauvaise réponse, consultez la règle...");
   gotoxy(x,y);
   textattr(RED ON BLACK);
   for (i=0; i<limit; i++) cprintf("°");
   gotoxy(x,y);
   cprintf("%s",String);
   if (Music==1) BipChoo();
}

//------------------------------------------------------------

void CompRep (char *String, char *Rep, char *Text, int x, int y, int limit)
{
	  static char StringMaj[40];
	  if (strcmp(String,Rep)==0)
	  {
	   Score++;
	   strcpy(Text,"Bonne réponse!");
	   gotoxy(x,y);
	   textcolor(BLUE);
	   cprintf("%s",String);
	   if( strlen(Rep) < limit )
	     {
	      LastBlank=1;
	      textcolor(BLACK);
	      movetext(x+limit+9,y+8,70,y+8,x+limit+8,y+8);   // fenêtre -> (10,9,70,18)
	     }
	   else LastBlank=0;
	   if (Music==1) BipYeah();
	  }
	else 
	{
	if ( (*Rep >= 'A') && (*Rep <= 'Z'))
	    {
		strcpy(StringMaj,String);
		*StringMaj = *String - 'a' + 'A';
		if (strcmp(StringMaj,Rep)==0)
			{
			Score++;
			strcpy(Text,"Attention à la majuscule...");
			gotoxy(x,y);
			textcolor(BLUE);
			cprintf(Rep);
			if( strlen(Rep) < limit )
			   {
			   LastBlank=1;
			   textcolor(BLACK);
			   movetext(x+limit+9,y+8,70,y+8,x+limit+8,y+8);   // fenêtre -> (10,9,70,18)
			   }
			else LastBlank=0;
			if (Music==1) BipYeah();
			}
		else MauvRep(String, Text, x, y, limit);
	    }
	else MauvRep(String, Text, x, y, limit);
	}
}
//------------------------------------------------------------

void RienPop(int x, int y, int limit)

{
   int i;
   textcolor(RED);
   for (i=0; i<limit; i++) cprintf("?");
   if (Music==1) BadLuck();
   Message("Vous n'avez rien entré...");
   window(10,9,70,18);
   textcolor(BLACK);
   textbackground(LIGHTGRAY);
   gotoxy(x,y);
   for (i=0; i<limit; i++) cprintf("°");
}

//------------------------------------------------------------

int ValidChoix (char Reponse, char *String)

{
int i,j,k;
static char Possible [20] [20]= {"Sa","Ça","sa","ça",
				 "La","Là","L'a","la","là","l'a",
				 "Peux","Peut","Peu","peux","peut","peu",
				 "ou","où","Ou","Où"};

   if ( (Reponse==1) || (Reponse==2) || (Reponse==3) )
     {j=0;k=3;}
   else if ( (Reponse==4) || (Reponse==5) || (Reponse==6) )
     {j=4;k=9;}
   else if ( (Reponse >= 7) && (Reponse <=11) )
     {j=10;k=15;}
   else if ( (Reponse >= 12) && (Reponse <=15) )
     {j=16;k=19;}
   
   for (i=j; i<=k; i++ )
      {
      if ( strcmp(String,&Possible[i][0])==0 ) return 1;
      }
   return 0;
}

//------------------------------------------------------------

void ValidPop(int x, int y, int limit, char *String)
{ 
  textcolor(RED);
  gotoxy(x,y);
  cprintf(String);
  if (Music==1) BadLuck();
  Message("Réponse non pertinente!");
  if (Modify==1) LastBlank=1;
  window(10,9,70,18);
  textattr(BLACK ON LIGHTGRAY);
  gotoxy(x,y);
  for (x=0; x<limit; x++) cprintf("°");  //utilisation de x pas rapport avec les coordonés
}

//------------------------------------------------------------

char WinGetString (int x, int y, int limit, char Reponse, int Endroit)
{
   char *String;
   static char Text[50];
   
   Modify=0;
   window(10,9,70,18);
   textcolor(BLACK);
   textbackground(LIGHTGRAY);
   
   if ( (Endroit==2) && (LastBlank==1) ) { LastBlank=0; Modify=1; x--;} // <- ici le bug!!!
   
   do   {
	String = getstring(x,y,limit);
	if (*String==0) RienPop(x,y,limit);
	}
   while (*String==0);

   if (*String==1) return 1 ;
   else
   {
      if( ValidChoix(Reponse,String)==0 ) {ValidPop(x,y,limit,String); return 2; }
      else
      {
      switch(Reponse)
	{
	case 1:  CompRep(String, "sa", Text, x, y, limit);   break;
	case 2:  CompRep(String, "ça", Text, x, y, limit);   break;
	case 3:  CompRep(String, "Sa", Text, x, y, limit);   break;
	case 4:  CompRep(String, "la", Text, x, y, limit);   break;
	case 5:  CompRep(String, "là", Text, x, y, limit);   break;
	case 6:  CompRep(String, "l'a", Text, x, y, limit);  break;
	case 7:  CompRep(String, "peut", Text, x, y, limit); break;
	case 8:  CompRep(String, "peux", Text, x, y, limit); break;
	case 9:  CompRep(String, "peu", Text, x, y, limit);  break;
	case 10: CompRep(String, "Peux", Text, x, y, limit); break;
	case 11: CompRep(String, "Peu", Text, x, y, limit);  break;
	case 12: CompRep(String, "ou", Text, x, y, limit); break;
	case 13: CompRep(String, "où", Text, x, y, limit);  break;
	case 14: CompRep(String, "Ou", Text, x, y, limit); break;
	case 15: CompRep(String, "Où", Text, x, y, limit);  break;
	}
      }
      Message(Text);
      Points();
   }
return 0;
}

//==============Exercices sur les homophones Ça et Sa======

void ExerSaCa(void)
{
	char itm=0, quit=0;
	char Pos [15] [4] =
	{{21,2,2,1},  //a      Reponse: sa = 1, ça = 2, Sa = 3
	 {32,2,2,2},  //a
	 {12,3,2,2},  //b
	 {43,3,2,1},  //b
	 {19,4,2,2},  //c
	 {19,5,2,2},  //d
	 {23,5,2,2},  //d
	 {7,6,2,3},   //e
	 {29,6,2,2},  //e
	 {22,7,2,2},  //f
	 {31,7,2,1},  //f
	 {25,8,2,2},  //g
	 {44,8,2,1},  //g
	 {24,9,2,1},  //h
	 {36,9,2,2}}; //h
	static char Texte [8] [80] = {"a)  Julie a mangé °° salade; °° lui a fait du bien.",
				      "b)  Tout °° me choque. Je déteste faire °° chambre.",
				      "c)  Sur le mur, °° et là, sont affichés de jolis dessins.",
				      "d)  Ne dis plus °°, °° me fait trop peur.",
				      "e)  °° robe est élégante; °° lui va bien.",
				      "f)  Pourquoi taire °° quand °° droiture est en jeu ?",
				      "g)  Elle se promenait °° et là, oubliant °° rancoeur.",
				      "h)  Il me contrôle à °° manière, °° me déplaît."};
	Score=0;
	LastBlank=0;
	Total=15;
	Option=1;
	ExerDesktop(Texte,0);
	for (itm=0; itm<15; itm++)
	    {
	    quit = WinGetString(Pos[itm][0],Pos[itm][1],Pos[itm][2],Pos[itm][3],1);
	    if (quit==1) return;
	    if (quit==2) {itm--; quit=0;}
	    }
	(tag_arr[0])=0xFB;
	Bravo();
}

//============== Homophones La, Là, L'a =======================
			
void ExerLaLa(void)
{
	char itm=0, quit=0;
	char Pos [12] [5] =
	{{18,2,3,5,1}, //a      Reponse: la = 4, là = 5, l'a = 6
	 {20,3,3,5,1}, //b       5ème item -> lequel sur la ligne
	 {14,4,3,6,1}, //c
	 {34,4,3,4,2}, //c
	 {22,5,3,5,1}, //d
	 {20,6,3,4,1}, //e
	 {36,6,3,4,2}, //e
	 {7,7,3,5,1}, //e
	 {21,8,3,6,1}, //f
	 {36,8,3,6,2}, //f
	 {14,9,3,4,1}, //g
	 {37,9,3,5,2}}; //g
	static char Texte [8] [80] = {"a)  Hugo était °°° et il patientait.",
				      "b)  Tu te places °°° où ça te plaît.",
				      "c)  Qui te °°° prêté ?  Est-ce °°° fille d'en face ?",
				      "d)  Après ce temps-°°°, tu es hors jeu.",
				      "e)  Ils ont bâti °°° maison dans °°° montagne,",
				      "    °°° où l'on voit le soleil se coucher.",
				      "f)  Le chirurgien °°° vérifié et °°° opéré.",
				      "g)  Où est °°° poubelle ?  Ici ou °°° ?"};
	Score=0;
	LastBlank=0;
	Total=12;
	Option=2;
	ExerDesktop(Texte,2);
	for (itm=0; itm<12; itm++)
	    {
	    quit = WinGetString(Pos[itm][0],Pos[itm][1],Pos[itm][2],Pos[itm][3],Pos[itm][4]);
	    if (quit==1) return;
	    if (quit==2) {itm--; quit=0;}
	    }
	(tag_arr[1])=0xFB;
	Bravo();
}

//============== Homophones Peut, Peux, Peu =======================
			
void ExerPeuPeu(void)
{
	char itm=0, quit=0;
	char Pos [15] [5] =
	{{7,2,4,10,1}, //a      Reponse: peut = 7, peux = 8, peu = 9
	 {29,2,4,8,2}, //a              Peux = 10, Peu = 11
	 {18,3,4,8,1}, //b       5ème item -> lequel sur la ligne
	 {48,3,4,9,2}, //b
	 {7,4,4,11,1}, //c
	 {14,4,4,9,2}, //c
	 {10,5,4,8,1}, //d
	 {30,5,4,9,2}, //d
	 {20,6,4,9,1}, //e
	 {32,6,4,9,2}, //e
	 {7,7,4,9,1}, //e
	 {14,7,4,9,2}, //e
	 {15,8,4,7,1}, //f
	 {18,9,4,9,1}, //g
	 {33,9,4,9,2}}; //g
	static char Texte [8] [80] = {"a)  °°°°-tu me laisser et °°°°-tu ne pas me déconcentrer ? ",
				      "b)  Certes, je °°°° réussir en me forçant un °°°°.",
				      "c)  °°°° à °°°°, son amour s'agrandit.",
				      "d)  Je °°°° te disputer un °°°°.",
				      "e)  Il discutait °°°°, riait °°°° et,",
				      "    °°°° à °°°°, il sombra dans la dépression.",
				      "f)  Comment °°°°-il atteindre son objectif ?",
				      "g)  L'étudiant °°°° motivé et °°°° studieux échoue."};
	Score=0;
	LastBlank=0;
	Total=15;
	Option=3;
	ExerDesktop(Texte,3);
	for (itm=0; itm<15; itm++)
	    {
	    quit = WinGetString(Pos[itm][0],Pos[itm][1],Pos[itm][2],Pos[itm][3],Pos[itm][4]);
	    if (quit==1) return;
	    if (quit==2) {itm--; quit=0;}
	    }
	(tag_arr[2])=0xFB;
	Bravo();
}
	
//====================== Homophones Ou, Où ===============================
			
void ExerOuOu(void)
{
	char itm=0, quit=0;
	char Pos [15] [5] =
	{{7,2,2,15}, //a      Reponse: ou = 12, où = 13
	 {47,2,2,12}, //a               Ou = 14, Où = 15
	 {7,3,2,15}, //b
	 {31,4,2,13}, //c
	 {11,5,2,12}, //d
	 {35,5,2,12}, //d
	 {7,6,2,14}, //e
	 {19,6,2,12}, //e
	 {15,7,2,13}, //f
	 {16,8,2,13}, //g
	 {39,9,2,12}}; //h
	static char Texte [8] [80] = {
				      "a)  °° est donc mon manteau, dans la maison °° ailleurs ?",
				      "b)  °° l'as tu perdu ?",
				      "c)  Je l'ai mis, je ne sais °°, j'espère le retrouver.",
				      "d)  Toi °° moi ?  Décidons-nous °° bien ce sera trop tard!",
				      "e)  °° l'Europe °° l'Asie, il vous faudra choisir.",
				      "f)  Le jour °° tu m'écouteras, tu comprendras mieux.",
				      "g)  L'époque °° l'on se détestait est révolu.",
				      "h)  Toute la classe viendra en vélo °° en autobus."};
	Score=0;
	LastBlank=0;
	Total=11;
	Option=4;
	ExerDesktop(Texte,0);
	for (itm=0; itm<11; itm++)
	    {
	    quit = WinGetString(Pos[itm][0],Pos[itm][1],Pos[itm][2],Pos[itm][3],Pos[itm][4]);
	    if (quit==1) return;
	    if (quit==2) {itm--; quit=0;}
	    }
	(tag_arr[3])=0xFB;
	Bravo();
}

//============== Message pour quitter===================

char EstTuSur(void)
   {
	char choix=1;
	int level;
	int buff[80][25];
	window(20,10,60,13);
	gettext(20,10,60,13,buff);

	for (level=8; level>=0; level--)
	{
	if (Music==1) sound((9-level)*80);
	delay(10);
	window(20+(2*level),10,59-(2*level),12);
	textcolor(WHITE);
	textbackground(RED);
	clrscr();
	}
	nosound();
	
	window(21,13,60,13);
	textbackground(BLACK);
	clrscr();
	window(60,11,60,13);
	textbackground(BLACK);
	clrscr();

	window(20,10,59,12);
	textcolor(WHITE);
	textbackground(RED);
	cprintf("\r\n  Voulez-vous vraiment quitter (O/N) ? ");
	do
	{
		 do
		   choix = getch();
		 while (choix==0);
		if ((choix=='o')||(choix=='O')) 
			{
			return 1;
			}
		else if ((choix=='n')||(choix=='N')||(choix==ESC)) 
			{
			if (Music==1) { sound(50); delay(2); nosound(); };
			window(20,10,60,13);
			puttext(20,10,60,13,buff);
			return 2;
			}
		
		else 
		{
		Bip();
		}
	}
	while (1);
   }

/*=============== GETCODE ========================*/

int getcode(void)
{
	while(1)
	{
	char frstcode;
	VideBuffer();
	frstcode = getch();
	if (frstcode == ESC) return (1);
	else 
	if (frstcode == ENTER) return (ENTER);
	else if (frstcode == 0) return (getch());
	}
}


/*=============== ACTION =========================*/

char action (int pos)
{
	char Quit=0;
	switch(pos)
		  {
		  case 0:
			ExerSaCa();
			break;
		  case 1:
			ExerLaLa();
			break;
		  case 2:
			ExerPeuPeu();
			break;
		  case 3:
			ExerOuOu();
			break;
		  case 4:
			Quit=EstTuSur();
			break;
		  }
	return(Quit);
}

//=============== DISPLAY =========================

void display (char *arr[], int size, int pos)
{
	int j;
	for (j=0; j<size; j++)
		{
		textattr(WHITE ON BLUE);
		if (j==pos)
		     textattr(BLUE ON LIGHTGRAY);
		gotoxy(4,j+4);
		cprintf("%c  %s", *(tag_arr+j),*(arr+j));
		}
}

/*==============SCREEN TOTAL===========================*/

void ScreenTotal (void)
{
int i, num=0;

for (i=1; i<=4; i++) if (scr_arr[i]!=200) num++;
if (num != 0)
  {
  window(20,17,60,17);
  textattr(BLACK ON CYAN);
  clrscr();
  cprintf("             Points obtenus");
  window(20,18,60,19+num);
  textattr(WHITE ON BLUE);
  clrscr();
  if (scr_arr[1]!=200) cprintf("\r\n  Exercices «sa, ça»:          %3d/100",scr_arr[1]);
  if (scr_arr[2]!=200) cprintf("\r\n  Exercices «la, là, l'a»:     %3d/100",scr_arr[2]);
  if (scr_arr[3]!=200) cprintf("\r\n  Exercices «peut, peux, peu»: %3d/100",scr_arr[3]);
  if (scr_arr[4]!=200) cprintf("\r\n  Exercices «ou, où»:          %3d/100",scr_arr[4]);
  }
}

//==============MENU PRINCIPAL==========================

void MenuPrinc(void)
{
static char *items[NUM] = /* menu items */
       {"Exercices sur les homophones  «ça, sa»          ",
	"Exercices sur les homophones  «la, là, l'a»     ",
	"Exercices sur les homophones  «peut, peux, peu» ",
	"Exercices sur les homophones  «ou, où»          ",
	"Quitter                                         ",};
int curpos=0, code=0, status=0;
		    
   while(code!=1)
	   {
	      if (status == 0)
		    {
		       desktop();
		       window(12,5,68,5);
		       textcolor(BLACK);
		       textbackground(CYAN);
		       clrscr();
		       cprintf("                     Menu principal");
		       ScreenTotal();
		       window(12,6,68,14);
		       textcolor(WHITE);
		       textbackground(BLUE);
		       clrscr();
		       display(items, NUM, curpos);
		       window(12,6,68,14);
		       //textcolor(WHITE);
		       //textbackground(BLUE);
		       VideBuffer();
		       Dactylo("\r\n  Choisissez une option parmi les suivantes:",10);
		       code = 0;
		       status = 1;
		    }
		    else display(items, NUM, curpos);
		    
		    code = getcode();
		    if (code==1)
			  {
			  code=EstTuSur();
			  window(12,6,68,13);
			  }     
		    
		    else
		    switch(code)
		       {
		       case U_ARRO:
			  if (curpos>0 ) --curpos;
			  else curpos=NUM-1;
			  if (Music==1) { sound(50); delay(2); nosound(); };
			  break;
		       case D_ARRO:
			  if (curpos<NUM-1) ++curpos;
			  else curpos=0; 
			  if (Music==1) { sound(50); delay(2); nosound(); };
			  break;
		       case ENTER:
			  if (curpos != NUM-1) status = 0;
			  code = action(curpos);
			  window(12,6,68,14);
			  break;
		       case FCT_1:
			  AideMenu();
			  window(12,6,68,14);
			  break;
		       case FCT_2:
			  ToggleSound();
			  window(12,6,68,14);
			  break;
		       };
		  }
}

//=============== Introduction du programme ==============

void Intro(void)
{
	int factor;
	textbackground(BLUE);
	for (factor=5; factor>=0; factor--)
	    {
	    window(20+(3*factor),10+(factor/2),60-(3*factor),15-(factor/2));
	    clrscr();
	    delay(25);
	    }
	textbackground(BLACK);
	window(61,11,61,16);
	clrscr();
	window(21,16,60,16);
	clrscr();
	

	textbackground(BLUE);
	textcolor(WHITE);
	window(20,10,60,15);
	
	Dactylo("\r\n  LES HOMOPHONES",25);
	Dactylo("\r\n\r\n  par Diane Roberge ",20);
	Dactylo("\r\n  et  Stéphane Lorrain            1995",20);
	if (!kbhit()) delay(1500);            //  <------ Changer délais
	else delay(1000);
}

//=============== PROGRAMME PRINCIPAL ====================

void main (void)
{
	int buff[80][25];
	int x,y,i;
	gettext(1,1,80,25,buff);
	x=wherex();
	y=wherey();
	
	PasCur(); //system("pcxview mouse.pcx");
	Intro();
	MenuPrinc();
	window(1,1,80,25);
	for (i=25; i>=1; i--)
	   {
	   puttext(1,i,80,25,buff);
	   if (Music==1) {
		sound(i*100);
		delay(8);
		sound(i*100-50);
		delay(8); }
	   else delay(18);
	   }
	nosound();
	VideBuffer();
	gotoxy(x,y);
	textattr(WHITE ON BLACK);
/*  
	cprintf("Ce programme peut être distribué gratuitement à condition");
	cprintf("\r\nqu'il ne soit modifié en aucune manière.");
	cprintf("\r\nSi vous avez des questions ou des commentaires concernant",10);
	cprintf("\r\nce programme, vous pouvez écrire à:",10);
	Dactylo("\r\n\r\n   7310 Christophe Colomb #402,",10);
	Dactylo("\r\n   Montréal (Qué),\r\n   H2R 3S6",10);
	VideBuffer();
*/
	SetCur();
}
